from django.urls import path

from .views import TestingPageView, ResultPageView

urlpatterns = [
    path('', TestingPageView.as_view(),  name='testing'),     # new 2 , method_HomePageView(views) and link_home
    path('result/', ResultPageView.as_view(),  name='result'),
]