from django.contrib import admin
from .models import Question, Choice, Result, ResultDescription

# Register your models here.


class ChoiceInLine(admin.TabularInline):
    model = Choice
    
    
class QuestionAdmin(admin.ModelAdmin):
    # какие поля выводить в админке
    list_display = ( 'parent_nomer', 'question_text', 'pub_date')
    # по каким полям производить поиск
    search_fields = ( 'parent_nomer', 'question_text')
    fieldsets = [
        ('Введите вопрос: "Допускается 500 символов"',                  {'fields': ['question_text'] }),
        ('Описание вопроса: "Допускается 500 символов"',                {'fields': ['question_description']}),
        ('Картинка вопроса:',                                           {'fields':['image']}),
        ('Номер вопроса из документа "Комплекс тестовых воросов на соответствие \
            квалификационным требованиям № 13245 от 20.07.2017" ',      {'fields': ['parent_nomer']}),
        ('Дата создания вопроса',                                       {'fields': ['pub_date']}),
       
    ]

    inlines = [ChoiceInLine]
    

class Description(admin.TabularInline):
    model = ResultDescription

class ResultAdmin(admin.ModelAdmin):
    list_display = ('user_text', 'result_pub_date')

    fieldsets = [
          ('Описание ответов от user:' ,  {'fields': ['user_text']}),
         
    ]
    inlines = [Description]

admin.site.register(Question, QuestionAdmin)
admin.site.register(Result, ResultAdmin)
