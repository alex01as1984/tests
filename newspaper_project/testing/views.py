from django.shortcuts import render
from django.views.generic import TemplateView   # new 2
# Create your views here.

class TestingPageView(TemplateView):       # inheritance in TemplateView
    template_name = 'testing/testing.html'


class ResultPageView(TemplateView):       # inheritance in TemplateView
    template_name = 'testing/result.html'