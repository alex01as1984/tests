from django.db import models
from django.shortcuts import reverse

# Create your models here.

class Question(models.Model):
    question_text = models.TextField(max_length=500, 
        verbose_name='Вопрос:')
    question_description = models.TextField(max_length=500, blank=True, 
        verbose_name='Описание вопроса:')
    image = models.ImageField(upload_to='static/testing/images', blank=True, 
        verbose_name='Картинка вопроса:')
    parent_nomer = models.IntegerField(default=0, unique=True, 
        verbose_name='Номер вопроса из документа:')
    pub_date = models.DateTimeField('date published') 
        
    def __str__(self):
        return self.question_text
        
    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)
    
    # создание метки для paginator
    class Meta:
        verbose_name_plural = 'Вопросы'
        verbose_name = 'Вопрос'
        ordering = ['parent_nomer']

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.TextField(max_length=500,
        verbose_name='Ответ:')
    votes = models.IntegerField(default=0,
        verbose_name='Правильно: 1 / не правильно: 0')

    def __str__(self):
        return self.choice_text


# результат
class Result(models.Model):
    # user
    user_text = models.CharField(max_length=150)                
    # время каждого вопроса на который он отвечал
    result_pub_date  = models.DateTimeField(auto_now_add=True, db_index=True)
    
    def __str__(self):
        return self.user_text
    
    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

    class Meta:
        verbose_name_plural = 'Результаты тестирования'

# описание результата
class ResultDescription(models.Model):
    result_user = models.ForeignKey(Result, on_delete=models.CASCADE)
    # номер по порядку
    d_id = models.IntegerField(default=0)
    # вопрос пользователю
    question_t = models.CharField(max_length=500)
    # ответ который выбрал пользователь
    question_choice= models.CharField(max_length=500, blank=True)
    # правильность ответа
    question_votes = models.CharField(max_length=30)
    # id вопроса для анализа ответа   
    nomer = models.IntegerField(default=0)

    def __str__(self):
        return self.question_t
